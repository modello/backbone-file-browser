define(function (require) {
	'use strict';
	var AppRouter = require('utils/AppRouter');
	var Vent = require('utils/Vent');

	return AppRouter.extend({

		routes: {
			'': 'onSelectNode',
			':nodeType/:nodeId': 'onSelectNode'
		},

		initialize: function(options) {

			AppRouter.prototype.initialize.apply(this, arguments);

			this.context = options.context;

			this.navOptions = {
				replace:false,
				routeFromSelf: false
			};

		},

		onSelectRoot: function() {
			this.onSelectNode('Folder', 0);
		},

		onSelectNode: function(nodeType, nodeId){
			
			if(!this.navOptions.routeFromSelf) {
				
				this.context.nodes.fetch()

				.done(_.bind(function(data) {
					
					this.context.nodes.setSelectedNode(parseInt(nodeId, 10) || 0);
					this.context.nodes.setWithChildrenOf(parseInt(nodeId, 10) || 0);

				}, this));

			}else {
				
				this.context.nodes.setSelectedNode(parseInt(nodeId, 10) || 0);
				this.context.nodes.setWithChildrenOf(parseInt(nodeId, 10) || 0);
			}

			Vent.region.trigger('Layout:changeView', 'gallery', { selectedType: nodeType });

		}

	});

});