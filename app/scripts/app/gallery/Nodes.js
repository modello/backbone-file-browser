define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require("underscore");
	var Backbone = require("backbone");
	var Node = require("./Node");
	
	return Backbone.Collection.extend({

		url: './data.json',

		model: Node,

		initialize: function() {
			this.settings = new Backbone.Model();
			this.settings.breadcrumbs = new Backbone.Collection();
			this.currentNodes = new Backbone.Collection([], {model: Node}); //used with Thumbnails view
			this.selectedNode = new Node(); //used with Detail view
		},

		setWithChildrenOf: function(folderId) {

			this.settings.set('currentParent', folderId);
			
			//todo:perhaps folderId can also be a string
			if(_.isNumber(folderId) ) {
				this._updateCurrentNodes(this._getChildrenOf(folderId));
				this.updateBreadcrumbs(folderId);
			}

		},

		setSelectedNode: function(nodeId) {

			this.selectedNode.clear({silent: true});
			var node = this.get(nodeId);

			if(node) {
				this.selectedNode.set(node.toJSON());
			}
			
		},

		_updateCurrentNodes: function(nodes) {

			this.currentNodes.reset(nodes);
			
		},

		_getChildrenOf: function(folderId) {

			return _.where(this.toJSON(), {parent:folderId});

		},

		updateBreadcrumbs: function(nodeId) {

			var crumbs = [];
			var node = this.get(nodeId);
			if(node) { 
				crumbs = this._getAncestors(node);
			}
			crumbs.push({id:0, title:"Home"});
			crumbs.reverse();
			this.settings.breadcrumbs.reset(crumbs);
			return crumbs;

		},

		_getAncestors: function(node) {

			var ancestors = [node.toJSON()];
			var collection = this;
		
			var addAncestor = function(node) {

				var ancestor = collection.findWhere({id: node.get('parent')});

				if(ancestor) {
					ancestors.push(ancestor.toJSON());
					addAncestor(ancestor);
				}

			};

			addAncestor(node);

			return ancestors;

		}
	
	});

});