define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require("underscore");
	var Backbone = require("backbone");
	var Marionette = require("marionette");
	var ThumbnailsView = require('./thumbnails/ThumbnailsView');
	var Detail = require('./detail/Detail');
	var AppDetail = require('./detail/AppDetail');
	var ConfigView = require('./ConfigView');
	var Vent = require('utils/Vent');
	
	return Backbone.Marionette.Layout.extend({

		className: 'gallery-layout',

		template: 'GalleryLayout',

		detailTemplates: {
			'Application': 'AppDetail',
			'Image': 'ImageDetail',
			'Movie': 'IFrameDetail',
			'Slides': 'SlidesDetail'
		},

		ui: {
			modal: '#modal',
			modalContent: '#modalContent'
		},

		regions: {
			configArea: '#configArea',
			contentArea: '#contentArea'
			
		},

		initialize: function(options) {

			this.collection = _.chk(function() { return options.context.nodes; });
			this.selectedType = options.selectedType;

		},

		updateConfigArea: function() {

			if(this.configView) {
				this.configView.off(null, null, this);
				this.configView.close();
			}
			
			this.configView = new ConfigView({model: this.collection.settings, collection: this.collection});
			//this.configView.on('clickParent', this.onClickParent, this);
			this.configView.on('crumbclick', this.onCrumbClick, this);
			this.configArea.show(this.configView);

		},

		updateContentArea: function() {

			if(this.currentSelectedType && this.currentSelectedType === this.selectedType) { return; }

			if(this.contentView) {
				this.contentView.off(null, null, this);
				this.contentView.close();
			}
			
			if(!this.selectedType || this.selectedType === 'Folder') {
				this.contentView = new ThumbnailsView({collection: this.collection.currentNodes});			
			}else {

				var DetailType = this.selectedType === 'Application' ? AppDetail : Detail;
				this.contentView = new DetailType({model: this.collection.selectedNode, template: this.detailTemplates[this.selectedType] });			
			}
			
			this.contentView.on('itemview:clickNode', this.onClickNode, this);
			this.contentArea.show(this.contentView);

			this.currentSelectedType = this.selectedType;

		},

		updateView: function(options) {

			if(options && options.selectedType) {
				this.selectedType = options.selectedType;
			}

			this.updateContentArea();

		},

		/*onClickParent: function(parentId) {

			var parentNode = this.collection.findWhere({id:parentId});
			if(parentNode) {
				this.onCrumbClick(parentNode.get('parent'));
			}
			
		},*/

		onClickNode: function(view, type) {

			this.onCrumbClick(view.model.id);
			
		},

		onCrumbClick: function(nodeId) {
			//this.collection.setWithChildrenOf(nodeId);
			var node = this.collection.get(nodeId);
			var type = node ? node.get('type') : '';
			var routePrependStr = type ? type+'/' : 'Folder/';

			Vent.route.trigger('navigateTo', routePrependStr + nodeId, {routeFromSelf: true});
		},

		onRender: function() {

			this.updateContentArea();
			this.updateConfigArea();

		},

		onClose: function() {

			this.contentView.off(null, null, this);
			this.contentView.close();
			
		}
	
	});
	
});