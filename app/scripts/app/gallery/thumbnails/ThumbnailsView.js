define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require("underscore");
	var Backbone = require("backbone");
	var Marionette = require("marionette");
	var ThumbnailView = require('./ThumbnailView');
	var createjs = require('createjs');
	
	var ThumbnailsView = Backbone.Marionette.CollectionView.extend({

		tagName: 'ul',

		className: 'thumbnails-view small-block-grid-2 medium-block-grid-3 large-block-grid-4',

		initialize: function() {
			
			this.preload = new createjs.LoadQueue(true, "./");
			this.preload.on("fileload", this.onFileLoad, this);
			this.preload.on("error", this.onFileError, this);
			this.preload.setMaxConnections(5);

		},

		onKeyDown: function(e) {
			console.log('onKeyDown', e.keyCode);
		},

		getItemView: function(item) {

			return ThumbnailView;
			
		},

		loadAssets: function() {

			var manifest = [];
			this.collection.each(function(model) {
				if(model.get('thumbnail')) {
					manifest.push({ id:model.get('id'), src: model.get('thumbnail')});
				}
			});
			
			if(manifest.length > 0) { this.preload.loadManifest(manifest); }
		},

		onFileLoad: function(e) {
			
			var thumbnail = this.children.findByModel(this.collection.findWhere({ id: e.item.id }));
			thumbnail.$el.removeClass('hidden');

		},

		onFileError: function(e) {
			//console.log('onFileError', e.item.id);

		},

		onBeforeRender: function() {

			this.loadAssets();

		},

		onClose: function() {
			this.preload.off();
			this.preload.close();
		}

	});
	
	return ThumbnailsView;
});
		