define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require("underscore");
	var Backbone = require("backbone");
	var Marionette = require("marionette");
	
	var ThumbnailView = Backbone.Marionette.ItemView.extend({

		template: 'ThumbnailView',

		tagName: 'li',

		className: 'hidden',

		events: {
			'click a': 'onClick'
		},

		onClick: function(e) {
			this.trigger('clickNode',  this.model.get('type'));
		}
	
	});
	
	return ThumbnailView;
});
		