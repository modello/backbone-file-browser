define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require("underscore");
	var Backbone = require("backbone");
	require('backbone-deep-model');
	
	return Backbone.DeepModel.extend({	
		defaults: {
			parent: 0
		}
	});
	
});
		