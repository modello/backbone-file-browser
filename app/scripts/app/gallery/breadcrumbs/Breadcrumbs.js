define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require("underscore");
	var Backbone = require("backbone");
	var Marionette = require("marionette");
	
	var Breadcrumb = Backbone.Marionette.ItemView.extend({

		tagName: 'li',

		template: 'Breadcrumb'
	
	});

	return Backbone.Marionette.CollectionView.extend({

		tagName: 'ul',

		itemView: Breadcrumb,

		className: 'breadcrumbs',

		events: {
			'click a': 'onClick'
		},

		onClick: function(e) {
			this.trigger('crumbclick', parseInt($(e.target).attr('id'), 10));
		}
	
	});
	
});