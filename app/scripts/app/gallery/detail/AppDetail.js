define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');
	var Marionette = require('marionette');
	var hljs = require('hljs');
	
	return Backbone.Marionette.Layout.extend({

		className: 'detail',

		template: 'AppDetail',

		ui: {
			tabs: '.tabs',
			tabsContent: '.tabs-content',
			videoLink: '#videoLink',
			imageLink: '#imageLink'
		},

		bindings: {
			'#title': 'title',
			'#imageLink': 'detail.imageLink',
			'#videoLink': 'detail.videoLink',
			'#description': 'detail.description',
			'#tabs': 'detail.tabs'
		},

		events: {
			'click a': 'onClickTab'
		},

		initialize: function(options) {
			this.model.on('change', this.render, this);
		},

		onClickTab: function(e) {

			this.ui.tabs.find('dd').removeClass('active');
			$(e.target).closest('dd').addClass('active');

			var id = $(e.target).attr('id');
			
			this.showPanel(id, $(e.target));

		},

		showPanel: function(id, $el) {

			var src = $el.attr('data-src');

			this.ui.tabsContent.find('.content').removeClass('active');
			this.ui.tabsContent.find('[id="'+id+'"]').addClass('active');

			if(id === 'panel-Code') {
				this.showCode(src);
			}else if(id === 'panel-Demo') {
				this.showApp(src, $el.attr('data-width'), $el.attr('data-height'));
			}else if(id === 'panel-Preview') {
				this.showPreview(src);
			}

		},

		showApp: function(src, w, h) {

			this.ui.videoLink.attr('src', src);
			if(w) { this.ui.videoLink.attr('width', w); }
			if(h) {	this.ui.videoLink.attr('height', h); }

		},	

		showPreview: function(src) {
			this.ui.imageLink.attr('src', src);
		},

		showCode: function(src) {

			$.ajax(src)

			.done(_.bind(function(data) {
				
				var $code = $('pre>code', this.$el);
				$code.html(hljs.highlightAuto(data).value);

			}, this));

		},

		onRender: function() {
			
			this.stickit();

			var $el = this.ui.tabs.find('.active').find('a');
			this.showPanel($el.attr('id'), $el);
			
		},

		onClose: function() {
			this.model.off(null, null, this);
		}

	});

});