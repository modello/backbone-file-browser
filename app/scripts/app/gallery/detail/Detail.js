define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');
	var Marionette = require('marionette');
	
	return Backbone.Marionette.Layout.extend({

		className: 'detail',

		ui: {
			iframe: 'iframe'
		},
		
		bindings:{
			'#title': 'title',
			'#imageLink': 'detail.imageLink',
			'#videoLink': 'detail.videoLink',
			'#description': 'detail.description',
			'#tabs': 'detail.tabs',
			'.contentDimensions': {
				observe: ['detail.width', 'detail.height'],
				update: function($el, vals, model) {
					if(vals[0]) { $el.attr('width', vals[0]); }
					if(vals[1]) { $el.attr('height', vals[1]); }
				}
			}
		},

		onRender: function() {

			this.stickit();
			this.focusContent();
			
		},

		focusContent: function() {
			setTimeout(_.bind(function() {
				var iframe = this.ui.iframe[0];
				if(iframe) { iframe.focus(); }
			}, this));
		}

	});

});