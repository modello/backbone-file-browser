define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require("underscore");
	var Backbone = require("backbone");
	var Marionette = require("marionette");
	var Breadcrumbs = require('./breadcrumbs/Breadcrumbs');
	
	return Backbone.Marionette.ItemView.extend({

		template: 'ConfigView',

		ui: {
			breadcrumbs: '#breadcrumbs'
		},

		/*events: {
			'click #parentBtn': 'onParentClick',
			'click #lightboxBtn': 'onLightboxClick'
		},

		bindings: {
			'#parentBtn': {
				observe: 'currentParent',
				update: function($el, val) {
					if(val === 0 ){
						$el.hide();
					}else {
						$el.show();
					}
				}
			}
		},

		onParentClick: function(e) {
			this.trigger('clickParent', this.model.get('currentParent'));
		},
		
		onLightboxClick: function(e) {
			this.trigger('lightbox', this.model);
		},*/

		onRender: function() {
			this.stickit();
			this.breadcrumbs = new Breadcrumbs({collection: this.collection.settings.breadcrumbs});
			this.breadcrumbs.on('crumbclick', this.onCrumbClick, this);
			this.breadcrumbs.setElement(this.ui.breadcrumbs);
		},

		onCrumbClick: function(nodeId) {
			this.trigger('crumbclick', nodeId);
		},

		onClose: function() {
			this.breadcrumbs.off(null, null, this);
			this.breadcrumbs.close();
		}
	
	});
	
});
		