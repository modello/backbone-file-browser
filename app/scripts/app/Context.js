define(function (require, exports, module) {
	'use strict';
	var Nodes = require('app/gallery/Nodes');
	
	var Context = function(){

		this.createContext();

	};

	Context.prototype = {

		createContext: function() {	

			window.nodes = this.nodes = new Nodes();

		}

	};
	
	return Context;
});