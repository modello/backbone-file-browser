define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery');
    var Marionette = require('marionette');
	var Backbone = require('backbone');
	var Vent = require('utils/Vent');
	var Context = require('./Context');
	var Layout = require('./Layout');
	var GalleryRouter = require('./gallery/Router');

	var Application = new Marionette.Application();

	var app = {

		init: function() {
			
			console.log("%c Build created by %s on %s", "color: blue;", module.config().developer, module.config().date);
			this.context = new Context();

		},

		setupRouters: function() {

			new GalleryRouter({ context: this.context });
			
		},

		hidePreloader: function() {

			$('#overlay').delay(500).fadeOut(500);

		},

		renderContainer: function() {

			new Layout({ context: this.context }).render().$el.insertAfter('#overlay');

		},

		start: function() {
			
			Backbone.history.start();
			//Vent.route.trigger('navigateTo', 'gallery');
			//Vent.region.trigger('Layout:changeView', 'gallery');
			
		}

	};
	
	Application.on('initialize:before', function(options) {

		app.hidePreloader();

	});

	Application.addInitializer(_.bind(function(options){

		app.init();
		app.renderContainer();
		app.setupRouters();

	}, this));

	Application.on('initialize:after', function(options) {

		app.start();

		//initialise foundation
		setTimeout(function(){

			$(document).foundation({
				tooltips: {
					selector : '.has-tip',
					additional_inheritable_classes : [],
					tooltip_class : '.tooltip',
					touch_close_text: 'tap to close',
					disable_for_touch: false
				}
			});
		});

	});

	return Application;

});