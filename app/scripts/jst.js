define(['handlebars'], function(Handlebars) {

this["JST"] = this["JST"] || {};

this["JST"]["Layout"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div id=\"overlayArea\" class=\"hide-overlay-area\"></div>\r\n<div id=\"contentArea\">Hello!</div>\r\n<div class=\"row\">\r\n  <div class=\"large-12 columns\">\r\n    <h1>Welcome to Foundation</h1>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"large-12 columns\">\r\n    <div class=\"panel\">\r\n      <h3>We&rsquo;re stoked you want to try Foundation! </h3>\r\n      <p>To get going, this file (index.html) includes some basic styles you can modify, play around with, or totally destroy to get going.</p>\r\n      <p>Once you've exhausted the fun in this document, you should check out:</p>\r\n      <div class=\"row\">\r\n        <div class=\"large-4 medium-4 columns\">\r\n      <p><a href=\"http://foundation.zurb.com/docs\">Foundation Documentation</a><br />Everything you need to know about using the framework.</p>\r\n    </div>\r\n        <div class=\"large-4 medium-4 columns\">\r\n          <p><a href=\"http://github.com/zurb/foundation\">Foundation on Github</a><br />Latest code, issue reports, feature requests and more.</p>\r\n        </div>\r\n        <div class=\"large-4 medium-4 columns\">\r\n          <p><a href=\"http://twitter.com/foundationzurb\">@foundationzurb</a><br />Ping us on Twitter if you have questions. If you build something with this we'd love to see it (and send you a totally boss sticker).</p>\r\n        </div>        \r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"large-8 medium-8 columns\">\r\n    <h5>Here&rsquo;s your basic grid:</h5>\r\n    <!-- Grid Example -->\r\n\r\n    <div class=\"row\">\r\n      <div class=\"large-12 columns\">\r\n        <div class=\"callout panel\">\r\n          <p><strong>This is a twelve column section in a row.</strong> Each of these includes a div.panel element so you can see where the columns are - it's not required at all for the grid.</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"large-6 medium-6 columns\">\r\n        <div class=\"callout panel\">\r\n          <p>Six columns</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"large-6 medium-6 columns\">\r\n        <div class=\"callout panel\">\r\n          <p>Six columns</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"large-4 medium-4 small-4 columns\">\r\n        <div class=\"callout panel\">\r\n          <p>Four columns</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"large-4 medium-4 small-4 columns\">\r\n        <div class=\"callout panel\">\r\n          <p>Four columns</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"large-4 medium-4 small-4 columns\">\r\n        <div class=\"callout panel\">\r\n          <p>Four columns</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    \r\n    <hr />\r\n            \r\n    <h5>We bet you&rsquo;ll need a form somewhere:</h5>\r\n    <form>\r\n      <div class=\"row\">\r\n        <div class=\"large-12 columns\">\r\n          <label>Input Label</label>\r\n          <input type=\"text\" placeholder=\"large-12.columns\" />\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"large-4 medium-4 columns\">\r\n          <label>Input Label</label>\r\n          <input type=\"text\" placeholder=\"large-4.columns\" />\r\n        </div>\r\n        <div class=\"large-4 medium-4 columns\">\r\n          <label>Input Label</label>\r\n          <input type=\"text\" placeholder=\"large-4.columns\" />\r\n        </div>\r\n        <div class=\"large-4 medium-4 columns\">\r\n          <div class=\"row collapse\">\r\n            <label>Input Label</label>\r\n            <div class=\"small-9 columns\">\r\n              <input type=\"text\" placeholder=\"small-9.columns\" />\r\n            </div>\r\n            <div class=\"small-3 columns\">\r\n              <span class=\"postfix\">.com</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"large-12 columns\">\r\n          <label>Select Box</label>\r\n          <select>\r\n            <option value=\"husker\">Husker</option>\r\n            <option value=\"starbuck\">Starbuck</option>\r\n            <option value=\"hotdog\">Hot Dog</option>\r\n            <option value=\"apollo\">Apollo</option>\r\n          </select>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"large-6 medium-6 columns\">\r\n          <label>Choose Your Favorite</label>\r\n          <input type=\"radio\" name=\"pokemon\" value=\"Red\" id=\"pokemonRed\"><label for=\"pokemonRed\">Radio 1</label>\r\n          <input type=\"radio\" name=\"pokemon\" value=\"Blue\" id=\"pokemonBlue\"><label for=\"pokemonBlue\">Radio 2</label>\r\n        </div>\r\n        <div class=\"large-6 medium-6 columns\">\r\n          <label>Check these out</label>\r\n          <input id=\"checkbox1\" type=\"checkbox\"><label for=\"checkbox1\">Checkbox 1</label>\r\n          <input id=\"checkbox2\" type=\"checkbox\"><label for=\"checkbox2\">Checkbox 2</label>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"large-12 columns\">\r\n          <label>Textarea Label</label>\r\n          <textarea placeholder=\"small-12.columns\"></textarea>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>     \r\n\r\n  <div class=\"large-4 medium-4 columns\">\r\n    <h5>Try one of these buttons:</h5>\r\n    <p><a href=\"#\" class=\"small button\">Simple Button</a><br/>\r\n    <a href=\"#\" class=\"small radius button\">Radius Button</a><br/>\r\n    <a href=\"#\" class=\"small round button\">Round Button</a><br/>            \r\n    <a href=\"#\" class=\"medium success button\">Success Btn</a><br/>\r\n    <a href=\"#\" class=\"medium alert button\">Alert Btn</a><br/>\r\n    <a href=\"#\" class=\"medium secondary button\">Secondary Btn</a></p>           \r\n    <div class=\"panel\">\r\n      <h5>So many components, girl!</h5>\r\n      <p>A whole kitchen sink of goodies comes with Foundation. Checkout the docs to see them all, along with details on making them your own.</p>\r\n      <a href=\"http://foundation.zurb.com/docs/\" class=\"small button\">Go to Foundation Docs</a>          \r\n    </div>\r\n  </div>\r\n</div>";
  });

this["JST"]["Breadcrumb"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<a id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a>";
  return buffer;
  });

this["JST"]["ConfigView"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<a id=\"parentBtn\" href=\"#\" class=\"small button\">Parent</a>\r\n<ul id=\"breadcrumbs\" class=\"breadcrumbs\"></ul>";
  });

this["JST"]["GalleryLayout"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"row\">\r\n	<div class=\"large-12 columns\">\r\n		<h1>Explorer</h1>\r\n	</div>\r\n</div>\r\n<div class=\"row\">\r\n	<div class=\"large-12 columns panel\">\r\n		<div class=\"row\">\r\n		    <div id=\"configArea\" class=\"large-12 columns\">\r\n		         navigation\r\n		    </div>\r\n	    </div>\r\n		<div class=\"row\">\r\n		    <div id=\"contentArea\" class=\"large-12 columns\">\r\n		         content\r\n		    </div>\r\n	    </div>\r\n	</div>\r\n</div>";
  });

this["JST"]["ThumbnailView"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " (parent ";
  if (stack1 = helpers.parent) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.parent; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ")  ";
  return buffer;
  }

  buffer += "<a class=\"th\">\r\nID:";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  if (stack1 = helpers.type) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.type; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  stack1 = helpers['if'].call(depth0, depth0.parent, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n</a>";
  return buffer;
  });

this["JST"]["GreetingScreen"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<br />\r\n";
  if (stack1 = helpers.greeting) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.greeting; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\r\n<br />\r\n";
  return buffer;
  });

this["JST"]["AlertBox"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n<div id=\"alert\" class=\"alert ";
  if (stack1 = helpers.context) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.context; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (stack1 = helpers.message) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.message; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</div>\r\n";
  return buffer;
  }

  stack1 = helpers['if'].call(depth0, depth0.message, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });

this["JST"]["Empty"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<td colspan=\"3\" class=\"empty\">\r\n<i class=\"fa fa-meh-o fa-4x\"></i>\r\n</td>";
  });

this["JST"]["Footer"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"span4\">\r\n	<div id=\"sizeOptions\" class=\"select dropup\" data-resize=\"auto\">\r\n		<button data-toggle=\"dropdown\" class=\"btn dropdown-toggle\">\r\n			<span id='selectedSize' class=\"dropdown-label\"></span>\r\n			<span class=\"caret\"></span>\r\n		</button>\r\n		<ul class=\"dropdown-menu\">\r\n			<li data-value=\"5\" data-selected=\"true\"><a>5</a></li>\r\n			<li data-value=\"10\"><a>10</a></li>\r\n			<li data-value=\"20\"><a>20</a></li>\r\n			<li data-value=\"50\"><a>50</a></li>\r\n			<li data-value=\"100\"><a>100</a></li>\r\n		</ul>\r\n	</div>\r\n</div>\r\n<div class=\"span8\">\r\n	<div id=\"offsetControls\" class=\"pagination-right\">\r\n		<span id=\"pageNumber\"></span>\r\n		<button id=\"firstPage\" class=\"btn\"><i class=\"fa fa-fast-backward\"></i></button>\r\n		<button id=\"previousPage\" class=\"btn\"><i class=\"fa fa-step-backward\"></i></button>\r\n		<button id=\"nextPage\" class=\"btn\"><i class=\"fa fa-step-forward\"></i></button>\r\n	</div>\r\n</div>";
  });

this["JST"]["Header"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<tr>\r\n  <th class=\"first\" style=\"width:10%;\">\r\n    <div class=\"th-inner\">Label 1</div>\r\n  </th>\r\n  <th>\r\n    <div class=\"th-inner\">Label 2</div>\r\n  </th>\r\n  <th>\r\n    <div class=\"th-inner\">Label 3</div>\r\n  </th>\r\n</tr>";
  });

this["JST"]["Row"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<td class=\"icon\" width=\"4%\">\r\n	sample\r\n</td>\r\n<td>sample</td>\r\n<td>sample</td>";
  });

this["JST"]["Table"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;


  buffer += "<div class=\"header-background\"></div>\r\n<div class=\"loading-background\">\r\n	<i class=\"fa fa-spinner fa-spin fa-4x\"></i>\r\n</div>\r\n<div class=\"users-view-container fixed-table-container-inner\">\r\n	<table class=\"tablesorter table table-bordered table-striped\">\r\n		<thead>\r\n		    ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.dynamicCompiledTemplate || depth0.dynamicCompiledTemplate),stack1 ? stack1.call(depth0, depth0.headerTemplate, options) : helperMissing.call(depth0, "dynamicCompiledTemplate", depth0.headerTemplate, options)))
    + "\r\n		</thead>\r\n		<tbody></tbody>\r\n	</table>\r\n</div>\r\n<div class=\"table-footer row-fluid\"></div>";
  return buffer;
  });

return this["JST"];

});