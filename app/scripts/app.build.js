require.config({

	paths: {
		'jquery': '../bower/jquery/jquery',
		'underscore': '../bower/underscore/underscore',
		'backbone': '../bower/backbone/backbone',
		'marionette': '../bower/marionette/lib/core/amd/backbone.marionette',
		'backbone.babysitter': '../bower/backbone.babysitter/lib/amd/backbone.babysitter',
		'backbone.wreqr': '../bower/backbone.wreqr/lib/amd/backbone.wreqr',
		'backbone.stickit': '../bower/backbone.stickit/backbone.stickit',
		'backbone-deep-model': '../bower/backbone-deep-model/distribution/deep-model',
		'backbone-route-filter': '../bower/backbone-route-filter/backbone-route-filter',
		'foundation': '../bower/foundation/js/foundation/foundation',
		'foundation.orbit': '../bower/foundation/js/foundation/foundation.orbit',
		'foundation.reveal': '../bower/foundation/js/foundation/foundation.reveal',
		'foundation.tooltip': '../bower/foundation/js/foundation/foundation.tooltip',
		'foundation.tab': '../bower/foundation/js/foundation/foundation.tab',
		'modernizr': '../bower/foundation/js/vendor/custom.modernizr',
		'handlebars': '../bower/handlebars/handlebars',
		'domReady': '../bower/requirejs-domready/domReady',
		'hljs': 'vendor/highlight/highlight.pack',
		'createjs': '../bower/PreloadJS/lib/preloadjs-0.4.1.min'
	},

	shim: {
		jquery : {
			exports : 'jQuery'
		},
		underscore : {
			exports : "_"
		},
		backbone: {
			deps: ["underscore", "jquery"],
			exports: "Backbone"
		},
		'backbone.stickit': ["underscore", "jquery", "backbone"],
		'backbone-deep-model': ["underscore", "jquery", "backbone"],
		'backbone-route-filter': ["backbone"],
		handlebars: {
			exports: 'Handlebars'
		},
		jst: {
			deps:[ "handlebars", "utils/handlebarhelpers"],
			exports: "JST"
		},
		foundation: {
			deps: ['jquery', 'modernizr'],
			exports: 'Foundation'
		},
		'foundation.orbit': {
			deps: ['jquery', 'foundation']
		},
		'foundation.reveal': {
			deps: ['jquery', 'foundation']
		},
		'foundation.tooltip': {
			deps: ['jquery', 'foundation']
		},
		'foundation.tab': {
			deps: ['jquery', 'foundation']
		},
		hljs: {
			exports: 'hljs'
		},
		createjs: {
			exports: 'createjs'
		}
		//'jquery.tablesorter':["jquery"]
	},

	packages: [
		{
			name:'app',
			main:'Application'
		}
	]

});