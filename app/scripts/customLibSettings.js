/**
	Place any function overrides, global settings, etc.. here
**/
define(function (require) {
	'use strict';
	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require("backbone");
	var Marionette = require("marionette");
	var Handlebars = require('handlebars');
	var JST = require("jst");
	var hljs = require('hljs');
	require('backbone.stickit');
	require('foundation');
	require('foundation.orbit');
	require('foundation.reveal');
	//require('foundation.tab');
	require('foundation.tooltip');

	//provides dynamic partial-like functionality (useful for injecting a template, chosen at runtime, into another template)
	Handlebars.registerHelper('compiledTemplate', function (templateName, data, opts) {
		
		if(!JST[templateName] && !_.isFunction(JST[templateName])) { return "Template '"+ templateName +"' is not precompiled in JST"; }

		return new Handlebars.SafeString(JST[templateName](data || {}));

	});

	//using precompiled handlebar templates, default was Underscore templates
	Backbone.Marionette.Renderer.render = function(template, data){

		if(!template) { return ""; }//makes template for a view optional

		if(!JST[template] && !_.isFunction(JST[template])) { return; }

		return JST[template](data);
	};

	//avoid caching on IE
	Backbone.$.ajaxSetup({ cache: false });

	Backbone.Stickit.addHandler({
		selector: 'img.stickit',
		update: function($el, val) { 
			$el.attr('src', val);
		},
		getVal: function($el) { return $el.attr('src'); }
	});

	Backbone.Stickit.addHandler({
		selector: 'iframe.stickit',
		update: function($el, val) { 
			$el.attr('src', val);
		},
		getVal: function($el) { return $el.attr('src'); }
	});

	Backbone.Stickit.addHandler({
		selector: 'code.stickit',
		events: ['change'],
		update: function($el, val) { 
			$el.html(hljs.highlightAuto($el.html()).value);
		}
	});

	//tabs
	Backbone.Stickit.addHandler({
		selector: 'dl.stickit',
		update: function($el, vals, model) {

			if(!vals) { return; }
			$el.html('');
			_.each(vals, function(val, i) {
				var active = i === 0 ? 'active': '';
				$el.append('<dd class="'+active+'" ><a id="panel-'+val.label+'" data-src="'+val.src+'" data-width="'+val.width+'" data-height="'+val.height+'">'+ val.label +'</a></dd>');
			});

		}
	});
	
	Backbone.Stickit.addHandler({
		selector: 'ul.stickit',
		events: ['append'],
		update: function($el, vals) { 
			_.each(vals, function(val) {
				$el.append('<li><img src="'+val+'"/></li>');
			});
			$el.trigger("append");
			setTimeout(function(){
				$el.foundation('orbit');
				if ($('.orbit-container', this.$el).length > 0) {
					$('.orbit-container', this.$el).height('510px');
				}
			});
		},
		getVal: function($el) { 
			var vals = [];
			$el.find('img').each(function(){
				vals.push($(this).attr('src'));
			});
			return vals; 
		}
	});

	_.mixin({
	
		// _.chk(function() { return options.context.nodes.currentNodes });
		chk: function (func) {
			try{
				return func();
			}catch(e){
				return null;
			}
		}
		
	});

});