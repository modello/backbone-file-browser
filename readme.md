## Synopsis

An example file browser made with Backbone and Marionette.

## Demo

[www.fboyle.com/files] [fboyle_www]

[fboyle_www]: http://www.fboyle.com/files
[fintangit_www]: https://github.com/Fintan

## Installation

Retrieve third party dependencies using **npm install** and **bower install**.

Run **grunt debug** to create a build in ./dist and view via localhost

(Note: I had to add 'window.Handlebars = Handlebars;' to '../bower/handlebars/handlebars.js' so that requirejs could locate the global Handlebars variable.)

## Contributors

Fintan Boyle ([https://github.com/Fintan] [fintangit_www])

## License

MIT