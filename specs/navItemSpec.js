define(function(require){
	'use strict';
	var $ = require('jquery');
	
	describe("Nav Item", function(){

		beforeEach(function() {
			
		});
		
		afterEach(function() {


		});

		it("is focusable when Enabled", function(){

			expect(0).toBe(0);
			//var widget = new Button();
			//assert(widget.isFocusable());


		});

		it("is not focusable when Disabled", function(){

			expect(0).toBe(0);
			//var widget = new Button();
			//widget.setDisabled(true);
			//assertFalse(widget.isFocusable());


		});

		it("can be focused", function(){

			expect(0).toBe(0);
			//var widget = new Button();
			//application.getRootWidget().appendChildWidget(widget);
			//widget.focus();
			//assert(widget.hasClass('focus'));

		});
		
		it("can switch Focus from another widget to itself", function(){

			expect(0).toBe(0);

			//var widget = new Button();
			//application.getRootWidget().appendChildWidget(widget);
			//var widget2 = new Button();
			//application.getRootWidget().appendChildWidget(widget2);

			//widget.focus();
			//assert(widget.hasClass('focus'));
			//assertFalse(widget2.hasClass('focus'));
			//widget2.focus();
			//assertFalse(widget.hasClass('focus'));
			//assert(widget2.hasClass('focus'));

		});

		it("can alter the Active and Focus States of other display objects based on its own focus state", function(){

			expect(0).toBe(0);

			//var container1 = new Container();
			//var button1 = new Button();
			//var button2 = new Button();
			//container1.appendChildWidget(button1);
			//container1.appendChildWidget(button2);

			//var container2 = new Container();
			//var button3 = new Button();
			//container2.appendChildWidget(button3);

			//application.getRootWidget().appendChildWidget(container1);
			//application.getRootWidget().appendChildWidget(container2);

			/*
			*     Application Root
			*     |
			*     '- Container  (Active Focus)
			*     |  |
			*     |  '- Button1 (Active Focus)
			*     |  '- Button2
			*     |
			*     '- Container2
			*        |
			*        '- Button3 (Active)
			*/

			//assert("1", container1.hasClass('active'));
			//assert("2", container1.hasClass('focus'));
			//assert("3", button1.hasClass('active'));
			//assert("4", button1.hasClass('focus'));
			//assertFalse("5", button2.hasClass('active'));
			//assertFalse("6", button2.hasClass('focus'));
			//assertFalse("7", container2.hasClass('active'));
			//assertFalse("8", container2.hasClass('focus'));
			//assert("9", button3.hasClass('active'));
			//assertFalse("10", button3.hasClass('focus'));

			//button1.focus();

			/*
			*     Application Root
			*     |
			*     '- Container  (Active Focus)
			*     |  |
			*     |  '- Button1 (Active Focus)
			*     |  '- Button2
			*     |
			*     '- Container2
			*        |
			*        '- Button3 (Active)
			*/

			/*assert("11", container1.hasClass('active'));
			assert("12", container1.hasClass('focus'));
			assert("13", button1.hasClass('active'));
			assert("14", button1.hasClass('focus'));
			assertFalse("15", button2.hasClass('active'));
			assertFalse("16", button2.hasClass('focus'));
			assertFalse("17", container2.hasClass('active'));
			assertFalse("18", container2.hasClass('focus'));
			assert("19", button3.hasClass('active'));
			assertFalse("20", button3.hasClass('focus'));
			*/

		});

		it("can be selected", function(){

			expect(0).toBe(0);

			//var button = new Button();
			//var bubbleEventSpy = this.sandbox.spy(button, 'bubbleEvent');
			//button.select();
			//assert(bubbleEventSpy.called);


		});

		it("can be selected using the Enter key", function(){

			expect(0).toBe(0);

			//var button = new Button();
			//var selectSpy = this.sandbox.spy(button, 'select');
			//button.fireEvent(new KeyEvent("keydown", KeyEvent.VK_ENTER));
			//assert(selectSpy.called);


		});

		it("has Focus classes", function(){

			expect(0).toBe(0);

			/*var button = new Button();
			application.getRootWidget().appendChildWidget(button);

			var button2 = new Button();
			application.getRootWidget().appendChildWidget(button2);

			assert(button.hasClass('buttonFocussed'));
			assertFalse(button.hasClass('buttonBlurred'));
			assertFalse(button2.hasClass('buttonFocussed'));
			assert(button2.hasClass('buttonBlurred'));

			button2.focus();

			assertFalse(button.hasClass('buttonFocussed'));
			assert(button.hasClass('buttonBlurred'));
			assert(button2.hasClass('buttonFocussed'));
			assertFalse(button2.hasClass('buttonBlurred'));
			*/

		});

		it("can remove Focus from itself", function(){

			expect(0).toBe(0);

			/*var button = new Button();
			application.getRootWidget().appendChildWidget(button);

			var button2 = new Button();
			application.getRootWidget().appendChildWidget(button2);

			button2.focus();

			assertFalse(button.hasClass('buttonFocussed'));
			assert(button.hasClass('buttonBlurred'));
			assert(button2.hasClass('buttonFocussed'));
			assertFalse(button2.hasClass('buttonBlurred'));

			button2.removeFocus();

			assertFalse(button.hasClass('buttonFocussed'));
			assert(button.hasClass('buttonBlurred'));
			assertFalse(button2.hasClass('buttonFocussed'));
			assert(button2.hasClass('buttonBlurred'));
			*/


		});

	});

});